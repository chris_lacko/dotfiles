#!/bin/bash

git config --global credential.helper cache
git config --global core.excludesFile $PWD/global_git_ignore.gitignore
